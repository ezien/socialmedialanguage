This directory contains the starter kit for CSE401 Homework 2, "Optimizing Query Engine"

- The query engine skeleton is in `src/q.ts`
- Compile by running `tsc` in the `hw2` directory.
  This will generate `src/q.js`.
- To run the tests in `test/test.js`, open `test/q.html` in your browser.
