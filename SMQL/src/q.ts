/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */

//// The AST

type fun = (l: any, r: any) => boolean;

// This class represents all AST Nodes
class ASTNode {
    type: string;


    constructor(type: string) {
        this.type = type;
    }

    execute(data1: any[], data2?: any[]): any {
        throw new Error("Unimplemented AST node " + this.type);
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 Implement call-chaining
    apply(f: (any) => any): ASTNode {
        var node = new ApplyNode(f);
        return new ThenNode(this, node);
    }

    filter(f: (any) => boolean): ASTNode {
        var node = new FilterNode(f);
        return new ThenNode(this, node);
    }

    count(): ASTNode {
        var node = new CountNode();
        return new ThenNode(this, node);
    }

    product(q: ASTNode): ASTNode {
        var node = new CartesianProductNode(this, q);
        return node;
    }


    join(q: ASTNode, f: fun | string): ASTNode {
        if (typeof f === "string") {
            var h = f as string;
            function g(l: any, r: any): boolean {
                return (typeof l[h] !== "undefined" && typeof r[h] !== "undefined" && l[h] === r[h])
            }
            let g2 = <FieldFunction> function(x) {
                return g(x.left, x.right);
            }
            g2.field = h;
            var cartesianNode = new CartesianProductNode(this, q);
            var filterNode = new FilterNode(g2);
            var applyNode = new ApplyNode(joinFunction);
            return new ThenNode(new ThenNode(cartesianNode, filterNode), applyNode);
        } else {
            var g = f as fun;
            var cartesianNode = new CartesianProductNode(this, q);
            var filterNode = new FilterNode((x) => g(x.left, x.right));
            var applyNode = new ApplyNode(joinFunction);
            return new ThenNode(new ThenNode(cartesianNode, filterNode), applyNode);
        }
        
    }
}

class HashJoinNode extends ASTNode {
    field: string;
    left: ASTNode;
    right: ASTNode;

    constructor(field: string, left: ASTNode, right: ASTNode) {
        super("HashJoin");
        this.field = field;
        this.left = left;
        this.right = right;
    }

    execute(data1: any[]): any;
    execute(data1: any[], data2: any[]): any;

    execute(data1: any[], data2?: any[]): any {
        if (data2) {
            return this.executeHelper(data1, data2);
        } else {
            return this.executeHelper(data1, data1);
        }
    }

    executeHelper(data1: any[], data2: any[]): any {
        let tableA = {};
        let tableB = {};
        let firstRes = this.left.execute(data1);
        let secondRes = this.right.execute(data2);

        for (let i = 0; i < firstRes.length; i++) {
            if (typeof firstRes[i][this.field] !== "undefined") {
                if (typeof tableA[firstRes[i][this.field]] === "undefined") {
                    tableA[firstRes[i][this.field]] = []
                }
                tableA[firstRes[i][this.field]].push(firstRes[i]);
            }
        }
        for (let i = 0; i < secondRes.length; i++) {
            if (typeof secondRes[i][this.field] !== "undefined") {
                if (typeof tableB[secondRes[i][this.field]] === "undefined") {
                    tableB[secondRes[i][this.field]] = []
                }
                tableB[secondRes[i][this.field]].push(secondRes[i]);
            }
        }

        let result = [];
        let firstKeys = Object.keys(tableA);
        for (let i = 0; i < firstKeys.length; i++) {
            if (typeof tableB[firstKeys[i]] !== "undefined") {
                let firstArr = tableA[firstKeys[i]];
                let secondArr = tableB[firstKeys[i]];
                for (let j = 0; j < firstArr.length; j++) {
                    for (let k = 0; k < secondArr.length; k++) {
                        let obj = {};
                        let aKey = Object.keys(firstArr[j]);
                        for (let l = 0; l < aKey.length; l++) {
                            obj[aKey[l]] = firstArr[j][aKey[l]];
                        }
                        let bKey = Object.keys(secondArr[k]);
                        for (let l = 0; l < bKey.length; l++) {
                            obj[bKey[l]] = secondArr[k][bKey[l]];
                        }
                        result.push(obj);
                    }
                }
            }
        }
        return result;
    }
}

// The Id node just outputs all records from input.
class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        var copy = [];
        for (var i = 0; i < data.length; i++) {
            copy.push(data[i]);
        }
        return copy;
    }
}

// The Filter node uses a callback to throw out some records
class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        var filteredCopy = [];
        for (var i = 0; i < data.length; i++) {
            if (this.predicate(data[i])) {
                filteredCopy.push(data[i]);
            }
        }
        return filteredCopy;
    }
}

// The Then node chains multiple actions on one data structure
class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        var arr = this.first.execute(data);
        return this.second.execute(arr);
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

// CountIf node
class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        var count = 0;
        for (var i = 0; i < data.length; i++) {
            if (this.predicate(data[i])) {
                count++;
            }
        }
        return [count];
    }
}

//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables

let theftsQuery = new FilterNode(x => x[13].match(/THEFT/));
let autoTheftsQuery = new FilterNode(x => x[13].match(/^VEH-THEFT/));

//// 1.3 Add Apply and Count Nodes
class ApplyNode extends ASTNode {
    predicate: (datum: any) => any;

    constructor(predicate: (any) => any) {
        super("Apply");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        var appliedCopy = [];
        for (var i = 0; i < data.length; i++) {
            appliedCopy.push(this.predicate(data[i]));
        }
        return appliedCopy;
    }
}

class CountNode extends ASTNode {

    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        var countCopy = [];
        var count = data.length;
        countCopy.push(count)
        return countCopy;
    }
}

//// 1.4 Clean the data

let cleanupQuery = new ApplyNode(x => ({type: x[13], description: x[15], date: x[17], area: x[19]}));

let Q = new IdNode();

//// 1.6 Reimplement queries with call-chaining

let cleanupQuery2 = Q.apply(x => ({type: x[13], description: x[15], date: x[17], area: x[19]})); 

let theftsQuery2 = Q.filter(x => x.type.match(/THEFT/)); 

let autoTheftsQuery2 = Q.filter(x => x.type.match(/^VEH-THEFT/)); 

//// 2.1 Optimize Queries

function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    let old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
        let newThis = old.call(this);
        return opt.call(newThis) || newThis;
    }
}

AddOptimization(ThenNode, function() {
    let thenNode = this as ThenNode;
    if (((thenNode.first instanceof ThenNode) && thenNode.first.second instanceof FilterNode) && (thenNode.second instanceof FilterNode)) {
        let firstFilter = thenNode.first.second as FilterNode;
        let secondFilter = thenNode.second as FilterNode;
        let newFilter = new FilterNode(x => (firstFilter.predicate)(x) && (secondFilter.predicate)(x));
        let newThen = new ThenNode(thenNode.first.first, newFilter);
        return newThen;
    }
});

// ...

//// 2.2 Internal node types and CountIf

AddOptimization(ThenNode, function() {
    let thenNode = this as ThenNode;
    if (((thenNode.first instanceof ThenNode) && thenNode.first.second instanceof FilterNode) && (thenNode.second instanceof CountNode)) {
        let filter = thenNode.first.second as FilterNode;
        let newThen = new ThenNode(thenNode.first.first, new CountIfNode(filter.predicate));
        return newThen;
    }
});

// ...

//// 3.1 Cartesian Products

class CartesianProductNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("CartesianProduct");
        this.first = first;
        this.second = second;
    }

    execute(data: any[]): any {
        let result = [];
        let firstRes = this.first.execute(data);
        let secondRes = this.second.execute(data)
        for (var i = 0; i < firstRes.length; i++) {
            for (var j = 0; j < secondRes.length; j++) {
                result.push({'left': firstRes[i], 'right': secondRes[j]});
            }
        }
        return result;
    }

    optimize(): ASTNode {
        return new CartesianProductNode(this.first.optimize(), this.second.optimize());
    }
}

// ...

//// 3.2 Joins

function joinFunction(x: any): any {
    var result = x.right;
    var leftKeys = Object.keys(x.left);
    for (var i = 0; i < leftKeys.length; i++) {
        if (typeof result[leftKeys[i]] === "undefined") {
            result[leftKeys[i]] = x.left[leftKeys[i]];
        }
    }
    return result;
}

// ...

//// 3.3 Optimizing joins

class JoinNode extends ASTNode {
    predicate: (datum: any) => boolean;
    first: ASTNode;
    second: ASTNode;

    constructor(predicate: (any) => boolean, first: ASTNode, second: ASTNode) {
        super("Join");
        this.predicate = predicate;
        this.first = first;
        this.second = second;
    }

    execute(data: any[]): any {
        let result1 = [];
        let firstRes = this.first.execute(data);
        let secondRes = this.second.execute(data);
        for (let i = 0; i < firstRes.length; i++) {
            for (let j = 0; j < secondRes.length; j++) {
                result1.push({'left': firstRes[i], 'right': secondRes[j]});
            }
        }
        var result2 = [];
        for (var i = 0; i < result1.length; i++) {
            if (this.predicate(result1[i])) {
                result2.push(joinFunction(result1[i]));
            }
        }
        return result2;
    }
}

AddOptimization(ThenNode, function() {
    let thenNode = this as ThenNode;
    if (((thenNode.first instanceof ThenNode) && thenNode.first.first instanceof CartesianProductNode && thenNode.first.second instanceof FilterNode) &&
            ((thenNode.second instanceof ApplyNode) && thenNode.second.predicate == joinFunction)) {
        let cartesian = thenNode.first.first as CartesianProductNode;
        if (isFieldFunction(thenNode.first.second.predicate)) {
            return new HashJoinNode(thenNode.first.second.predicate.field, cartesian.first, cartesian.second);
        }
        let filter = thenNode.first.second as FilterNode;
        let newNode = new JoinNode(filter.predicate, cartesian.first, cartesian.second);
        return newNode;
    }
});

// ...

//// 3.4 Join on fields
// ...

//// 3.5 Implement hash joins
// ...

//// 3.6 Optimize joins on fields to hash joins

AddOptimization(JoinNode, function() {
    let joinNode = this as JoinNode;
    if (isFieldFunction(joinNode.predicate)) {
        return new HashJoinNode(joinNode.predicate.field, joinNode.first, joinNode.second);
    }
});

// ...


interface FieldFunction {
    (any): any;
    field: string;
}

function isFieldFunction(f: any): f is FieldFunction {
    return ("field" in f) && (typeof f.field === "string");
}