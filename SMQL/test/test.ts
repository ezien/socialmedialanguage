///<reference path="../src/q.ts"/>
mocha.ui("bdd");

function cleanData() {
    let out = [];
    for (let i = 0; i < window.rawData.length; i++) {
        const x = window.rawData[i];
        out.push({type: x[13], description: x[15], date: x[17], area: x[19]});
    }
    return out;
}

describe("Problem 1", function () {
    it("Executing queries", function () {
        const q = new ThenNode(
            new IdNode(),
            new FilterNode(x => x[0] % 2 == 1));
        const out = q.execute(window.rawData);

        const good = [];
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][0] % 2) {
                good.push(window.rawData[i]);
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.deep.equal(good);
    });

    it("Write a query", function () {
        const good1 = [];
        const good2 = [];
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][13].match(/THEFT/)) {
                good1.push(window.rawData[i]);
            }
            if (window.rawData[i][13].match(/^VEH-THEFT/)) {
                good2.push(window.rawData[i]);
            }
        }

        const out1 = theftsQuery.execute(window.rawData);
        const out2 = autoTheftsQuery.execute(window.rawData);

        chai.expect(out1).to.deep.equal(good1);
        chai.expect(out2).to.deep.equal(good2);
    });

    it("Add Apply and Count nodes", function () {
        function even(x) {
            return (x % 2) === 0;
        }

        const q = new ThenNode(
            new ApplyNode(x => x[0]),
            new ThenNode(
                new FilterNode(even),
                new CountNode()));
        const out = q.execute(window.rawData);

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            if (even(window.rawData[i][0])) good++;
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });
    

    it("Clean the data", function () {
        const out = cleanupQuery.execute(window.rawData);
        chai.expect(out).to.deep.equal(cleanData());
    });

    it("Implement a call-chaining interface", function () {
        const q = Q.apply(x => x[0] % 2)
            .filter(x => !!x)
            .count();
        const out = q.execute(window.rawData);

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][0] % 2) good++;
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });
    

    it("Reimplement queries with call-chaining", function () {
        const data = cleanData();
        const out = cleanupQuery2.execute(window.rawData);
        chai.expect(out).to.deep.equal(data);

        const good1 = [];
        const good2 = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].type.match(/THEFT/)) {
                good1.push(data[i]);
            }
            if (data[i].type.match(/^VEH-THEFT/)) {
                good2.push(data[i]);
            }
        }


        const out1 = theftsQuery2.execute(data);
        const out2 = autoTheftsQuery2.execute(data);
        chai.expect(out1).to.deep.equal(good1);
        chai.expect(out2).to.deep.equal(good2);
    });
});

describe("Problem 2", function () {
    /* UNCOMMENT when filter call-chaining is implemented */
    it("Optimize filters", function () {
        const q = Q.filter(x => !!x).filter(x => !!x);
        const q2 = q.optimize();
        chai.expect(q2.type).to.equal("Then");
        chai.expect((q2 as ThenNode).first.type).to.equal("Id");
        chai.expect((q2 as ThenNode).second.type).to.equal("Filter");
    });

    it("Internal node types and CountIf", function () {
        const q = Q.filter(x => !!(x[0] % 2)).count();
        const q2 = q.optimize();
        chai.expect(q2.type).to.equal("Then");
        chai.expect((q2 as ThenNode).first.type).to.equal("Id");
        chai.expect((q2 as ThenNode).second.type).to.equal("CountIf");
    });
});

describe("Problem 3", function () {
    /* UNCOMMENT when CartesianProductNode is implemented */
    it("Cartesian products", function () {
        const testData = ['a', 'b', 'c'];
        const testExpected = [
            {left: 'a', right: 'a'},
            {left: 'a', right: 'b'},
            {left: 'a', right: 'c'},
            {left: 'b', right: 'a'},
            {left: 'b', right: 'b'},
            {left: 'b', right: 'c'},
            {left: 'c', right: 'a'},
            {left: 'c', right: 'b'},
            {left: 'c', right: 'c'}
        ];

        const q = new CartesianProductNode(new IdNode(), new IdNode());
        const out = q.execute(testData);
        const l = testData.length;
        chai.expect(out).to.have.length(l * l);
        chai.expect(out).to.deep.equal(testExpected);
    });

    /* UNCOMMENT when join call-chaining is implemented */
    it("Joins", function () {
        const q = Q.join(Q, (l, r) => l[0] == r[0] - 1).apply(x => x[0]);
        const out = q.execute(window.rawData);
        const l = window.rawData.slice(1);
        chai.expect(out).to.deep.equal(l.map(x => x[0]));
    });

    it("Joins Objects", function () {
        const q = Q.join(Q, (l, r) => true);
        const data = [{a: 0, b: 2}];
        const expected = [{a: 0, b: 2}];

        chai.expect(q.execute(data)).to.deep.equal(expected);
        chai.expect(q.optimize().execute(data)).to.deep.equal(expected);
    });

    it("Optimizing joins", function () {
        const q = Q.join(Q, function (l, r) {
            return l[0] == r[0] - 1
        });
        const outQ = q.optimize();
        chai.expect(outQ.type).to.equal("Join");

        const out = outQ.execute(window.rawData);
        chai.expect(out).to.deep.equal(q.execute(window.rawData));
    });
    it("Joins on fields", function () {
        const q = Q.join(Q, "type").count();
        const out = q.execute(cleanData());

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            for (let j = 0; j < window.rawData.length; j++) {
                if (window.rawData[i][13] == window.rawData[j][13]) good++;
            }
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });

    it("Implement hash joins", function () {
        const q = new HashJoinNode("type", new IdNode(), new IdNode()).count();
        const out = q.execute(cleanData());

        let good = 0;
        for (let i = 0; i < window.rawData.length; i++) {
            for (let j = 0; j < window.rawData.length; j++) {
                if (window.rawData[i][13] == window.rawData[j][13]) good++;
            }
        }

        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.have.length(1);
        chai.expect(out[0]).to.equal(good);
    });

    it("Join on two different tables", function () {
        const data1 = [{id: 123456, salary: 72000, year: 2016},
                       {id: 123456, salary: 75000, year: 2017},
                       {id: 654321, salary: 75000, year: 2017}];
        const data2 = [{id: 123456, name: 'john doe'},
                       {id: 654321, name: 'jane doe'},
                       {id: 111111, name: 'john smith'}];
        const q = Q.join(Q, 'id').optimize();
        console.log(q.execute(data1, data2));
    });

    /* UNCOMMENT when join call-chaining is implemented */
    it("Allows duplicate records in HashJoin", function () {
        const q = Q.join(Q, "a");
        const datum = {a: 3, b: 2};
        const data = [datum, datum];

        chai.expect(q.execute(data)).to.have.length(4);
        chai.expect(q.optimize().execute(data)).to.have.length(4);
    });

    it("Optimize joins on fields to hash joins", function () {
        const q = Q.join(Q, "type");
        const out = q.optimize();
        chai.expect(out.type).to.equal("HashJoin");
    });

    // Checks for all vehicle thefts at 14XX Block of NW Market Street
    it("Query 1", function () {
        const q =
        new ThenNode(
            new ThenNode(
                new IdNode(), 
                new FilterNode(x => x[13].match(/^VEH-THEFT/))),
            new FilterNode(x => x[15].match(/14XX BLOCK OF NW MARKET ST/)));
        const out = q.execute(window.rawData);

        const good = [];
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][13].match(/^VEH-THEFT/) && window.rawData[i][15].match(/14XX BLOCK OF NW MARKET ST/)) {
                good.push(window.rawData[i]);
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.deep.equal(good);
    });

    // Returns all types of thefts at 38XX BLOCK OF 24 AV S
    it("Query 2", function () {
        const q =
        new ThenNode(
            new ThenNode(
                new IdNode(), 
                new FilterNode(x => x[13].match(/^VEH-THEFT/) || x[13].match(/THEFT/))),
            new CountIfNode(x => x[15].match(/38XX BLOCK OF 24 AV S/)));
        const out = q.execute(window.rawData);

        const good = [];
        for (let i = 0; i < window.rawData.length; i++) {
            if (window.rawData[i][13].match(/^VEH-THEFT/) && window.rawData[i][15].match(/14XX BLOCK OF NW MARKET ST/)) {
                good.push(window.rawData[i]);
            }
        }
        chai.expect(out).to.be.an("Array");
        chai.expect(out).to.deep.equal([good.length]);
    });

    // Returns all cartesian nodes will left equal to a
    it("Query 3", function () {
        const testData = ['a', 'b', 'c'];
        const testExpected = [8];

        const q = 
        new ThenNode(
            new ThenNode(
                new ThenNode(
                    new CartesianProductNode(new IdNode(), new IdNode()),
                    new FilterNode(x => x.left.match('a'))),
                new CountNode()),
            new ApplyNode(x => x + 5));

        const out = q.execute(testData);
        const l = testData.length;
        chai.expect(out).to.have.length(1);
        chai.expect(out).to.deep.equal(testExpected);
    });

    // Returns the right nodes of the cartesian products that have left node a and right node not a
    it("Query 4", function () {
        const testData = ['a', 'b', 'c', 'd'];
        const testExpected = ['b', 'c', 'd'];

        const q = 
        new ThenNode(
            new ThenNode(
                new ThenNode(
                    new CartesianProductNode(new IdNode(), new IdNode()),
                    new FilterNode(x => x.left.match('a'))),
                new FilterNode(x => !x.right.match('a'))),
            new ApplyNode(x => x = x.right));

        const out = q.execute(testData);
        const l = testExpected.length;
        chai.expect(out).to.have.length(l);
        chai.expect(out).to.deep.equal(testExpected);
    });

    // fun way to use (and test) a join to see how many vehicle thefts happened in the UW area
    it("Query 5", function() {
        const q = 
        new HashJoinNode("area", 
            new ThenNode(
                new IdNode(),
                new FilterNode(x => x.area.includes('XX BLOCK') && parseInt(x.area.split('XX BLOCK')[0]) > 30 
                                        && parseInt(x.area.split('XX BLOCK')[0]) < 60)),
            new ThenNode(
                new IdNode(),
                new FilterNode(x => x.description === 'VEHICLE THEFT')));

        let clean = cleanData();

        let match = []
        for (let i = 0; i < clean.length; i++) {
            if (clean[i].area.includes('XX BLOCK') && parseInt(clean[i].area.split('XX BLOCK')[0]) > 30 
                    && parseInt(clean[i].area.split('XX BLOCK')[0]) < 60 && clean[i].description === 'VEHICLE THEFT') {
                match.push(clean[i]);
            }
        }
        const out = q.execute(clean);

        chai.expect(out).to.deep.equal(match);
    });

});
