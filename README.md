# README #

Preproposal Link: https://docs.google.com/document/d/1r5Q9TdDIoFsscP-aorD8RrMx5U_BUTxXWlZkWd4VdfI/edit?usp=sharing

Proposal Link: https://docs.google.com/document/d/1CSAjMCEpEvebkLPgcf_Qs_Ih5_Ncsh76Oq63DFamfhI/edit?usp=sharing

Design Document Link: https://docs.google.com/a/uw.edu/document/d/18Y9Torsu_bp7jczwLbdV4DFQ4jmkEDPL_PyAi2zueIc/edit?usp=sharing

Report Link: https://docs.google.com/document/d/173_89Ra0we6gETvF4Oedh9iOEo5nA0QjqiiGjlqxd6M/edit?usp=sharing

Poster Link: https://docs.google.com/a/uw.edu/presentation/d/1u_vebJwwl12ZCeFXlepA_UmjeQTa-vKi-Dk5QltUiJ8/edit?usp=sharing

Demo Link: https://drive.google.com/a/uw.edu/file/d/1p_cfPKbidb3CHqoQ4B7o41K57MipAKZe/view?usp=sharing

### What is this repository for? ###
This project is a querying language that will analyze data from different social media languages such as Facebook and Twitter.

### How do I get set up? ###
Tutorial:
You have the data set from either Facebook and/or Twitter and you can apply different functions to each node. The social media nodes will be specifically for the social media content and other nodes can be applied to focus on a more specfic data set or analytic. 
Call Chaining will be used to stack each command. Note commands will be read from the order they are called.

Example Programs:
Program 1:  
// join two twitter queries (not too exciting)

let sq1 = S.twitter().fromUser('k24dizzle');
let sq2 = S.twitter().withQuery('michaelcao311');
let q1 = Q.join(Q, 'user').optimize();
sq1.execute(10, function(x) {
    sq2.execute(10, function(y) {
        console.log(q1.execute(x, y));
    });
});

Program 2: 
// join facebook location query and a hashtag twitter query
let sq3 = S.twitter().withQuery('#gohawks');
let sq4 = S.facebook().withQuery('centurylink').withType('place').withField('single_line_address');
let q2 = Q.join(Q, function(x, y) {
    if (typeof y.single_line_address === 'undefined' || typeof x.user.location !== 'string') return false;
    else {
         return x.user.location.toLowerCase().indexOf('seattle') > -1 && y.single_line_address.toLowerCase().indexOf('seattle') > -1;
    }
}).optimize();
setTimeout(function() {
    sq3.execute(100, function(x) {
        sq4.execute(100, function(y) {
            console.log(q2.execute(x, y));
        })
    });
}, 3000);

Program 3:
// livestream of cat tweets
setTimeout(function() {
    let sqStream = S.twitter().withQuery('cat').stream(function(tweet) {
        console.log(tweet);
    });
}, 10000);